import copy, sys
from players import Learner

class Board:
    ends = [
        (0, 1, 2), (3, 4, 5), (6, 7, 8), # rows
        (0, 3, 6), (1, 4, 7), (2, 5, 8), # columns
        (0, 4, 8), (2, 4, 6)             # diagonals
    ]

    def __init__(self):
        self.map = [ 0 for i in range(9) ]
        self.final = False

    def isOver(self):
        if self.getWinnerId() == 0:
            for end in self.ends:
                mapline = self.getMapline(end)
                for i in range(3):
                    if mapline[i] == 0:
                        return False
        return True

    def getMapline(self, end):
        return (self.map[end[0]], self.map[end[1]], self.map[end[2]])

    def getWinnerId(self):
        for end in self.ends:
            mapline = self.getMapline(end)
            if mapline == (1, 1, 1):
                return 1
            elif mapline == (2, 2, 2):
                return 2
        return 0

    def getFeatures(self, pid):
        oid = 2 if pid == 1 else 1
        features = [0 for i in range(6)]
        for end in self.ends:
            mapline = self.getMapline(end)
            n = [0, 0, 0]
            for i in range(3):
                n[mapline[i]] += 1
            features[0] += int(n[pid] == 3)
            features[1] += int(n[oid] == 3)
            features[2] += int(n[pid] == 1 and n[0] == 2)
            features[3] += int(n[oid] == 1 and n[0] == 2)
            features[4] += int(n[pid] == 2 and n[0] == 1)
            features[5] += int(n[oid] == 2 and n[0] == 1)
        return features
 
    def update(self, move, pid):
        if self.map[move] != 0:
            raise Exception("CellNotEmpty")
        else:
            self.map[move] = pid

    def undo(self, move):
        if self.map[move] == 0:
            self.print()
            raise Exception("Trying to undo a move (%d) not played" % move)
        else:
            self.map[move] = 0

    def print(self):
        print("     0  1  2")
        print("     -------")
        for i in range(0, 9, 3):
            sys.stdout.write(" ")
            sys.stdout.write(str(int(i / 3)))
            sys.stdout.write(" |")
            for j in range(3):
                if self.map[i + j] == 0:
                    s = "_"
                elif self.map[i + j] == 1:
                    s = "X"
                else:
                    s = "O"
                sys.stdout.write(" ")
                sys.stdout.write(s)
                sys.stdout.write(" ")
            sys.stdout.write("\n")
        sys.stdout.write("\n")

class Game:
    def __init__(self, player1, player2, board):
        self.player1 = player1
        self.player2 = player2
        if player1.id == player2.id:
            raise Exception("Players have the same id")
        self.board = board
        self.history = []
        self.over = False
        self.winnerId = 0

    def run(self, verbose = False):
        player = self.player2
        while not self.isOver():
            if player == self.player2:
                player = self.player1
            else:
                player = self.player2
            self.history.append(copy.deepcopy(self.board))
            move = player.getMove(self.board)
            self.board.update(move, player.id)
        self.history.append(copy.deepcopy(self.board))
        self.over = True
        self.winnerId = self.board.getWinnerId()
        if verbose:
            print(self.getState())
        if type(self.player1) == Learner and self.player1.learning:
            self.player1.learn(self)
        if type(self.player2) == Learner and self.player2.learning:
            self.player2.learn(self)

    def isOver(self):
        return self.board.isOver()

    def getState(self):
        if self.winnerId == -1:
            return "Not finished"
        elif self.winnerId == 0:
            return "Draw"
        elif self.winnerId == 1:
            return "Player 1 won"
        else:
            return "Player 2 won"

