import sys, copy, random
from game import Board, Game
from players import Human, Learner, RandomPlayer, Champion

def printStat(count):
    print(count)
    total = count[0] + count[1] + count[2]
    if total > 0:
        stat = []
        for i in range(len(count)):
            stat.append(int(count[i] * 100 / total))
        print(stat)

def play(player1, player2, n, verbose = False):
    step = n / 10
    count = [0, 0, 0]
    for i in range(1, n):
        if i % step == 0 and verbose:
            printStat(count)
        game = Game(player1, player2, Board())
        game.run(verbose)
        count[game.winnerId] += 1
    printStat(count)

if __name__ == "__main__":

    player1 = Champion(1)
    player3 = Learner(1)
    player2 = Learner(2)
    #player2.weights = [1, 10000, -10000, 30, -30, 700, -6000]

    play(player3, player2, 500)

    player = Human(1)

    play(player, player2, 10)

    #player2.learning = False
    
    #play(player2, player1, 100)
