import copy, random

class Player:
    def __init__(self, id):
        self.id = id

    def getMove(self, board):
        raise NotImplementedError("Maybe this object is too abstract")
    
    def getPossibleMoves(self, board):
        moves = []
        for i in range(9):
            if board.map[i] == 0:
                moves.append(i)
        return moves

class Human(Player):
    def getMove(self, board):
        board.print()
        ok = False
        while not ok:
            print("Player {id}".format(**{'id': self.id}))
            move = int(input("move > "))
            if move >= 0 and move < 9:
                if board.map[move] == 0:
                    ok = True
                else:
                    print("Error: CellNotEmpty. Try again")
            else:
                print("Error: must be between 0 and 8. Try again")
        return move

class Learner(Player):
    nu = 0.1

    def __init__(self, id, learning = True):
        self.weights = [ (random.random() - 0.5) for i in range(7) ]
        self.id = id
        self.learning = learning

    def getMove(self, board):
        moves = self.getPossibleMoves(board)
        if len(moves) < 1:
            raise Exception(
                "No possible move. Are you sure game is not over?")
        bestMove = None
        bestValue = None
        for move in moves:
            board.update(move, self.id)
            value = self.evaluate(board)
            if bestValue == None or value > bestValue:
                bestValue = value
                bestMove = move
            board.undo(move)
        return bestMove

    
    def evaluateF(self, board):
        features = board.getFeatures(self.id)
        features.insert(0, 1)
        if not len(features) == len(self.weights):
            raise Exception(
                "Number of weights and features must be the same")
        value = 0
        for i in range(len(features)):
            value += self.weights[i] * features[i]
        return (value, features)
    
    def evaluate(self, board):
        value, features = self.evaluateF(board)
        return value

    def learn(self, game):
        if not game.over:
            raise Exception("Can't train on a game that is not over")
        tes = self.getTE(game.history)
        self.updateWeights(tes)

    
    def getTE(self, history):
        tes = []
        endBoard = history[len(history) - 1]
        endBoardValue = self.evaluateEndBoard(endBoard)
        tes.append((endBoard, endBoardValue))
        for i in range(len(history) - 1):
            board = history[i]
            value = self.evaluate(history[i + 1])
            tes.append((board, value))
        return tes

    def evaluateEndBoard(self, board):
        winner = board.getWinnerId()
        if winner == 0:
            return 0
        elif winner == self.id:
            return 100
        else:
            return -100

    def updateWeights(self, tes):
        for te in tes:
            value, features = self.evaluateF(te[0])
            tmp = (te[1] - value) * self.nu
            for i in range(len(self.weights)):
                self.weights[i] += tmp * features[i]

class RandomPlayer(Player):
    def getMove(self, board):
        moves = self.getPossibleMoves(board)
        return moves[random.randint(0, len(moves) - 1)]

class Champion(Player):
    def __init__(self, id):
        self.id = id
        self.oid = 1 if self.id == 2 else 2

    def getMove(self, board):
        move, score = self.max(board)
        return move

    def max(self, board):
        bestScore = None
        bestMove = None
        moves = self.getPossibleMoves(board)
        for move in moves:
            board.update(move, self.id)

            if board.isOver():
                score = self.evaluate(board)
            else:
                mtrash, score = self.min(board)

            if bestScore == None or score > bestScore:
                bestScore = score
                bestMove = move
            board.undo(move)
        return (bestMove, bestScore)
    
    def min(self, board):
        bestScore = None
        bestMove = None
        moves = self.getPossibleMoves(board)
        for move in moves:
            board.update(move, self.oid)

            if board.isOver():
                score = self.evaluate(board)
            else:
                mtrash, score = self.max(board)

            if bestScore == None or score < bestScore:
                bestScore = score
                bestMove = move
            board.undo(move)
        return (bestMove, bestScore)

    def evaluate(self, board):
        winnerId = board.getWinnerId()
        if winnerId == 0:
            return 0
        elif winnerId == self.id:
            return 1
        else:
            return -1

